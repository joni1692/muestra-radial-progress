import 'package:flutter/material.dart';
import 'package:radial_progress/src/pages/home_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Radial Progress App',
      home: HomePage()
    );
  }
}