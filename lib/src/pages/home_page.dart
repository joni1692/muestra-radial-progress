import 'package:flutter/material.dart';
import 'package:radial_progress/src/widgets/radial_progress.dart';
import 'package:radial_progress/src/widgets/wave_header.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  double porcentaje = 0.0;

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            height: size.height * 0.30,
            child: WaveHeader(color: Colors.pink),
          ),
          Expanded(
            child: Center(
              child: Container(
                width: 200,
                height: 200,
                child: RadialProgress(
                  porcentaje: porcentaje,
                  colorPrimario: Colors.pink,
                ),
              )
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            // simulamos que el porcentaje aumento un 10%
            porcentaje += 10;

            // si llegamos a 100% volvemos a 0
            if(porcentaje > 100) {
              porcentaje = 0;
            }
          });
        },
        child: Icon(Icons.refresh),
      ),
    );
  }
}