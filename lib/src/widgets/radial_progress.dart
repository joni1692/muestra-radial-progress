import 'dart:math';

import 'package:flutter/material.dart';

class RadialProgress extends StatefulWidget {

  /// numero que va del 0.0 al 100.0, especifica cuanto del arco de progreso hay que pintar
  final double porcentaje;
  /// color del arco que va pintando el porcentaje de progreso
  final Color colorPrimario;
  /// color para el ciruculo de fondo
  final Color colorSecundario;
  /// grosor del arco que va pintando el porcentaje de progreso
  final double grosorPrimario;
  /// grosor del circulo de fondo
  final double grosorSecundario;

  const RadialProgress({
    @required this.porcentaje, 
    this.colorPrimario = Colors.blue,
    this.colorSecundario = Colors.grey,
    this.grosorPrimario = 10.0,
    this.grosorSecundario = 4.0
  });

  @override
  _RadialProgressState createState() => _RadialProgressState();
}

class _RadialProgressState extends State<RadialProgress> with SingleTickerProviderStateMixin {

  AnimationController controller;
  double porcentajeAnterior;

  @override
  void initState() {
    
    // al inicio el porcentaje anterior va a ser igual al que recibimos por parametro
    porcentajeAnterior = widget.porcentaje;

    controller = AnimationController(vsync: this, duration: Duration( milliseconds: 200));

    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    //
    controller.forward(from:0.0);

    //
    final diferenciaAnimar = widget.porcentaje - porcentajeAnterior;
    porcentajeAnterior = widget.porcentaje;

    return AnimatedBuilder(
      animation: controller,
      builder: (BuildContext context, Widget child) {
        return Container(
          padding: EdgeInsets.all(10.0),
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: _MiRadialProgress( 
              (widget.porcentaje - diferenciaAnimar) + diferenciaAnimar * controller.value,
              widget.colorPrimario,
              widget.colorSecundario,
              widget.grosorPrimario,
              widget.grosorSecundario
              ),

          ),
        );
      },
    );
  }
}

class _MiRadialProgress extends CustomPainter{

  final porcentaje;
  final Color colorPrimario;
  final Color colorSecundario;
  final double grosorPrimario;
  final double grosorSecundario;


  _MiRadialProgress(
    this.porcentaje,
    this.colorPrimario,
    this.colorSecundario,
    this.grosorPrimario,
    this.grosorSecundario
  );

  @override
  void paint(Canvas canvas, Size size) {
    
    /*
    * dibujamos el circulo secundario
    */
    final paint = Paint()
      ..strokeWidth = grosorSecundario
      ..color = colorSecundario
      ..style = PaintingStyle.stroke;

    final center = Offset(size.width * 0.5, size.height * 0.5);
    final radio = min(size.width * 0.5, size.height * 0.5);

    canvas.drawCircle(center, radio, paint);

    /*
    * el circulo primario lo dibujamos a partir de un arco que cuando llegue al 100% sera 
    * un circulo superpuesto al circulo secundario
    */
    final paintArco = Paint()
      ..strokeWidth = grosorPrimario
      ..color = colorPrimario
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

      double arcAngle = 2 * pi * (porcentaje / 100);
      canvas.drawArc(
        Rect.fromCircle(center:center, radius: radio), 
        -pi/2, 
        arcAngle, 
        false, 
        paintArco
      );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}